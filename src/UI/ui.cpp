#include <iostream>

namespace UI {
	static std::string mainMenu = "Hey look, it's the main menu!";

	void displayMainMenu() {
		std::cout << mainMenu << std::endl;
	}
}
