BUILD_ARGS = -Wall -Werror -Isrc/


build:
	g++ src/main.cpp -Ldist/* $(BUILD_ARGS) -o enigma


libraries: 
	mkdir -p dist/
	for dir in src/*/; do\
		cd $$dir; \
		g++ -c *.cpp ; \
		mv *.o ../../dist/ ; \
	done


clean:
	rm -rf dist/ enigma
